# -*- perl -*-

package Topbloke;

use strict;
use warnings;

use POSIX;
use IO::File;
use IPC::Open2;
use File::Path qw(make_path remove_tree);
use File::Basename;

BEGIN {
    use Exporter   ();
    our ($VERSION, @ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);

    $VERSION     = 1.00;
    @ISA         = qw(Exporter);
    @EXPORT      = qw(debug $tiprefs $baserefs %known_metadata
		      run_git run_git_1line run_git_check_nooutput
		      run_git_test_anyoutput git_get_object
		      git_config git_dir chdir_toplevel enable_reflog
                      check_no_metadata foreach_unknown_metadata
                      check_clean_tree
		      setup_config 
		      current_branch parse_patch_name parse_patch_spec
		      patch_matches_spec
		      foreach_patch
		      metafile_process depsfile_add_dep
		      wf_start wf wf_abort wf_done wf_contents
		      closeout);
    %EXPORT_TAGS = ( );
    @EXPORT_OK   = qw();
}

our $git_command = 'git';

our %known_metadata;

sub debug ($) {
    my ($msg) = @_;
    print STDERR "DEBUG: $msg\n" or die $!;
}

#----- general interaction with git -----

sub run_git {
    # takes optional prefix arguments:
    #    coderef    hook to call for each line read,
    #                with $_ containing chomped line; if not supplied,
    #                output is not read
    #    scalarref  place to store exit status; if not supplied,
    #                nonzero exit status is fatal
    my ($estatusr,$linecallr);
    while (ref $_[0]) {
	my $ref = shift @_;
	if (ref $ref eq 'SCALAR') {
	    $estatusr = $ref;
	} elsif (ref $ref eq 'CODE') {
	    $linecallr = $ref;
	} else {
	    die ref($ref)." @_ ?";
	}
    }
    open GIT, "-|", $git_command, @_ or die $!;
    if ($linecallr) {
	while (<GIT>) {
	    chomp or die "$git_command @_ gave $_ ?";
	    $linecallr->();
	}
	GIT->eof or die $!;
    }
    if (!close GIT) {
	die "$git_command @_ $!" if $!;
	die unless $?;
	die "$git_command @_ ($?)" unless $estatusr;
	$$estatusr = $?;
    } else {
	$$estatusr = 0 if $estatusr;
    }
}

sub run_git_1line {
    my $l;
    run_git(sub { $l = $_; }, @_);
    die "git @_ ?" unless defined $l;
    return $l;
}

sub run_git_check_nooutput {
    my ($what) = shift @_;
    run_git(sub { die "$what $_\n"; }, @_);
}

sub run_git_test_anyoutput {
    my $any = 0;
    run_git(sub { $any=1; }, @_);
    return $any;
}

sub git_get_object ($) {
    my ($objname) = @_;
    our ($gro_pid, $gro_out, $gro_in);
    if (!$gro_pid) {
	$gro_pid = open2($gro_out, $gro_in, $git_command, qw(cat-file --batch))
	    or die $!;
    }
    #debug("git_get_object $objname");
    $SIG{'PIPE'} = 'IGN';
    print $gro_in $objname,"\n" or die $!;
    $gro_in->flush or die "$objname $!";
    $SIG{'PIPE'} = 'DFL';
    my $l = <$gro_out>;
    chomp $l or die "$objname $l ?";
    #debug("git_get_object $objname => $l");
    if ($l =~ m/ missing$/) {
	return 'missing';
    } elsif (my ($type,$bytes) = $l =~ m/^\S+ (\w+) (\d+)$/) {
	my $data = '';
	if ($bytes) {
	    (read $gro_out, $data, $bytes) == $bytes or die "$objname $!";
	}
	my $nl;
	(read $gro_out, $nl, 1) == 1 or die "$objname $!";
	$nl eq "\n" or die "$objname ?";
	return ($type, $data);
    } else {
	die "$objname $l";
    }
}

sub git_config ($$) {
    my ($cfgvar, $default) = @_;
    my ($l, $estatus);
    run_git(\$estatus, sub { 
	die if defined $l; 
	$l = $_; },
	    qw(config), $cfgvar);
    if (defined $l) {
	die "$cfgvar ($estatus)" if $estatus;
	return $l;
    } else {
	die "$cfgvar ($estatus)" unless $estatus==0 || $estatus==256;
	return $default;
    }
}

sub git_dir () {
    our $git_dir;
    if (!defined $git_dir) {
	$git_dir = run_git_1line(qw(rev-parse --git-dir));
    }
    return $git_dir;
}

#----- specific interactions with git -----

sub chdir_toplevel () {
    my $toplevel;
    run_git(sub { $toplevel = $_; }, 
	    qw(rev-parse --show-toplevel));
    die "not in working tree?\n" unless defined $toplevel;
    chdir $toplevel or die "chdir toplevel $toplevel: $!\n";
}

sub enable_reflog ($) {
    my ($branchref) = @_;
    $branchref =~ m#^refs/# or die;
    my $logsdir = git_dir().'/logs/';
    my $dirname = $logsdir.dirname($branchref);
    make_path($dirname) or die "$dirname $!";
    open REFLOG, '>>', $logsdir.$branchref or die "$logsdir$branchref $!";
    close REFLOG or die $!;
}    

sub check_no_metadata ($) {
    # for checking foreign branches aren't contaminated
    my ($gitbranch) = @_;
    run_git_check_nooutput('foreign unexpectedly contains',
			   qw(ls-tree --name-only),
			   "$gitbranch:",
			   qw(.topbloke));
}

sub foreach_unknown_metadata ($$) {
    my ($ref, $code) = @_;
    # Examines $ref.
    # Executes $code for each tolerable unknown metadata found, with
    # $_ being the (leaf) name of the metadata file
    run_git(sub {
	die unless s#^\.topbloke/##;
	next if $known_metadata{$_};
	m/-$/ or die "found unsupported metadata in $ref; you must upgrade\n";
	$code->();
	    },
	    qw(ls-tree --name-only -r HEAD: .topbloke));
}

sub check_clean_tree ($) {
    run_git_check_nooutput("operation requires working tree to be clean",
			   qw(diff --name-only HEAD --));
    run_git_check_nooutput("operation cannot proceed with staged changes",
			   qw(diff --cached --name-only HEAD --));
}

$known_metadata{$_}=1 foreach qw(msg patch base deps deleted
                                 +included +ends);

#----- configuring a tree -----

sub setup_config () {
    my (@files) = (qw(lwildcard- msg patch base deps deleted
                      +iwildcard- +included +ends));
    my $version = 1;
    my $drvname = sub {
	my ($file) = @_;
	$file =~ s/^\+//;
	$file =~ s/\-$//;
	return $file;
    };
    foreach my $iteration (qw(0 1)) {
	foreach my $file (@files) {
	    my $cfgname = "merge.topbloke-".$drvname->($file);
	    my ($current, $current_estatus);
	    run_git(\$current_estatus,
		    sub { $current = $_; },
		    qw(config), "$cfgname.driver");
	    $current = "## failed $current_estatus" if $current_estatus;
	    next if $current =~ m/^topbloke-merge-driver --v$version /o;
	    die "$file $current ?" if $iteration;
	    debug("setting merge driver $file");
	    run_git(qw(config), "$cfgname.name",
		    "topbloke merge driver for $file");
	    run_git(qw(config), "$cfgname.driver",
		    "topbloke-merge-driver --v$version".
		    " $file %O %A %B %L");
	}
	my ($newattrsprefix, $newattrs, $attrsfile);

	my $attrs = '';
	my @needupdate;
	foreach my $file (@files) {
	    my ($pat,$check) = ($file, $file);
	    if ($file =~ m/wildcard/) {
		$pat = ($file =~ m/^\+/ ? '+' : '[^+]').'*';
		$check =~ s/\w.*/xxxunknown/ or die;
	    }
	    $pat = ".topbloke/$pat";
	    $check = ".topbloke/$check";
	    my $want = "topbloke-".$drvname->($file);
	    $attrs .= "$pat\tmerge=$want\n";
	    my $current = run_git_1line(qw(check-attr merge), $check);
	    $current =~ s#^\Q$check\E: merge: ## or die "$file $current ?";
	    next if $current eq $want;
	    die "$file $current ?" unless 
		$current eq 'unspecified' ||
		$current =~ m/^topbloke-\wwildcard$/;
	    push @needupdate, "$file=$current";
	}
	if (@needupdate) {
	    $attrsfile = git_dir()."/info/attributes";
	    my $newattrsf = new IO::File "$attrsfile.tmp", 'w'
		    or die "$attrsfile.tmp: $!";
	    die "@needupdate ?" if $iteration;
	    if (!open OA, '<', "$attrsfile") {
		die "$attrsfile $!" unless $!==ENOENT;
	    } else {
		while (<OA>) {
		    next if m#^\.topbloke/#;
		    print $newattrsf $_ or die $!;
		    print $newattrsf "\n" or die $! unless chomp;
		}
		die $! if OA->error;
		die $! unless close OA;
	    }
	    print $newattrsf $attrs or die $!;
	    close $newattrsf or die $!;
	    rename "$attrsfile.tmp", "$attrsfile" or die $!;
	}
    }
}

#----- branch and patch specs and parsed patch names -----

our $tiprefs = 'refs/topbloke-tips';
our $baserefs = 'refs/topbloke-bases';

sub current_branch () {
    open R, git_dir().'/HEAD' or die "open HEAD $!";
    my $ref = <R>;  defined $ref or die $!;
    close R;
    chomp $ref or die;
    if ($ref !~ s#^ref: ##) {
	return {
	    Kind => 'detached',
	    Ref => $ref,
	};
    }
    if ($ref =~ m#^refs/topbloke-(tip|base)s/([^/\@]*)\@([^/\@]*)/([^/]*)/#) {
	my $fullname = "$2\@$3/$4/$'";
	my $v = {
	    Kind => $1,
	    Email => $2,
	    Domain => $3,
	    Date => $4,
	    Nick => $', #',
	    Ref => $ref,
	    DepSpec => $fullname,
	    Fullname => $fullname,
	};
	return $v;
    } elsif ($ref =~ m#^refs/heads/#) {
	return {
	    Kind => 'foreign',
	    Ref => $ref,
	    DepSpec => "- $ref",
	};
    } else {
	return {
	    Kind => 'weird',
	    Ref => $ref,
	};
    }
}

sub parse_patch_name ($) {
    my ($patch) = @_;
    my ($eaddr, $date, $nick) = split /\//, $patch, 3;
    defined $nick && length $nick or die "$patch ?";
    my ($email, $domain) = $eaddr =~ m/^(.*)\@([^\@]+)$/
	or die "$patch eaddr ?";
    return {
	Email => $email,
	Domain => $domain,
	Date => $date,
	Nick => $nick,
	Kind => 'tip',
	DepSpec => $patch,
	Fullname => $patch,
	Ref => "refs/topbloke-tips/$patch",
    };
}

sub parse_patch_spec ($) {
    my ($orig) = @_;
    local $_ = $orig;
    warn 'FORMAT has new spec syntax nyi';
    my $spec = { }; # Email Domain DatePrefix DateNear Nick
    my $set = sub {
	my ($key,$val,$whats) = @_;
	die "multiple $whats in patch spec\n" if exists $spec->{$key};
	$spec->{$key} = $val;
    };
    my $rel_levels;
    for (;;) {
	if (s#([^/\@]*)\@([^/\@]*)/##) {
	    $set->('Email', $1, "email local parts") if length $1;
	    $set->('Domain', $2, "email domains") if length $1;
	} elsif (s#([^/]*\~[^/]*)/##) {
	    my $dspec = $1;
	    $dspec =~ y/~/ /;
	    open DATE, "-|", 'date','+%s','-d',$dspec or die $!;
	    my $l = <DATE>;
	    close DATE or die "date parsing failed\n";
	    chomp $l or die;
	    $set->('DateNear', $l, 'nearby dates');
	} elsif (s#^([0-9][^/]*)/##) {
	    my $dspec = $1;
	    $dspec =~ 
      m/^\d{4}(?:-\d\d(?:-\d\d(?:T(?:\d\d(?:\d\d(?:\d\d(?:Z)?)?)?)?)?)?)?$/
		or die "bad date prefix \`$dspec'\n";
	    $set->('DatePrefix', $dspec, 'date prefixes');
	} elsif (s#^\./##) {
	    $rel_levels ||= 1;
	} elsif (s#^\.\./##) {
	    $rel_levels ||= 1;
	    $rel_levels++;
	} else {
	    last;
	}
    }
    if (defined $rel_levels) {
	my $branch = current_branch();
	if (!defined $branch->{Nick}) {
	    die "relative patch spec \`$orig',".
		" but current branch not a topbloke patch\n";
	}
	my ($ceaddr,$cdate,@l) = split /\//, $branch->{Nick};
	@l >= $rel_levels or
	    die "relative patch spec \`$orig' has too many ../s\n";
	$_ = (join '/', @l[0..$#l-$rel_levels]).'/'.$_;
    } elsif (length) {
	$spec->{Nick} = $_;
    }
    return $spec;
}

sub patch_matches_spec ($$) {
    my ($parsedname, $spec) = @_;
    foreach my $k (qw(Email Domain Nick)) {
	debug("patch_matches_spec  mismatch $k"), return 0
	    if defined $spec->{$k} &&
	       $parsedname->{$k} ne $spec->{$k};
    }
    debug("patch_matches_spec  mismatch DatePrefix"), return 0
	if defined $spec->{DatePrefix} &&
	   substr($parsedname->{Date}, 0, length $spec->{DatePrefix})
	       ne $spec->{DatePrefix};
    debug("patch_matches_spec  match"), return 1;
}

#----- reading topbloke metadata -----

sub foreach_patch ($$$$) {
    my ($spec, $deleted_ok, $want, $body) = @_;
    # runs $body->($patch, $parsedname, \%meta)
    # where $meta{<metadata filename>} is, for <metadata filename> in @$want:
    #              undefined if metadata file doesn't exist
    #              defined with contents of file
    # and $parsedname is only valid if $spec is not undef
    #  (say $spec { }  if you want the name parsed but no restrictions)
    # entries in want may also be "<metadata filename>_"
    #  which means "strip trailing newlines" (result key in %meta is the same)
    # <metadata filename> may instead be "B_<metadata filename>"
    #  which means to look in the corresponding base branch
    my @want = @$want;
    my $atfront = sub {
	my ($thing) = @_;
	@want = ($thing, grep { $_ ne $thing } @want);
    };
    $atfront->(' patch');
    $atfront->('deleted') unless $deleted_ok;
    run_git(sub {
	debug("foreach_patch considering $_");
	m/ / or die "$_ ?";
	my $objname = $`;
	my %meta;
	my $parsedname;
	my $patch = substr($',19); #');
	my $wantix = 0;
	foreach my $wantent (@want) {
	    my $file = $wantent;
	    my $stripnl = ($file =~ s/_$//);
	    my $key = $file;
	    my $inbase = ($file =~ s/^B_//);

	    if ($file eq ' patch') {
		if ($spec) {
		    $parsedname = parse_patch_name($patch);
		    if (!patch_matches_spec($parsedname, $spec)) {
			debug("foreach_patch  mismatch");
			return;
		    }
		}
		next;
	    }

	    my $objkey = (!$inbase ? "$objname" : 
			  "$baserefs/$patch").":.topbloke/$file";
	    my ($got, $data) = git_get_object($objkey);
	    if ($got eq 'missing') {
		$meta{$key} = undef;
	    } elsif ($got eq 'blob') {
		$meta{$key} = $data;
		if ($file eq 'deleted' && !$deleted_ok) {
		    debug("foreach_patch  Deleted");
		    return;
		}
	    } else {
		warn "patch $patch object $objkey has unexpected type $got!\n";
		return;
	    }
	}
	debug("foreach_patch  YES $patch");
	$body->($patch, $parsedname, \%meta);
	    },
	    qw(for-each-ref --format), '%(objectname) %(refname)',
	        qw(refs/topbloke-tips));
}

#----- updating topbloke metadata -----

sub metafile_process ($$$$$) {
    my ($metafile, $startcode, $linecode, $endcode, $enoentcode) = @_;
    # runs $startcode->($outwf) at start
    # runs $linecode->($outwf) for each old line, with $_ the chomped line
    #   may modify $_, which will be written to $outf
    # at end runs $endcode->($outwf);
    # runs $enoentcode->($outwf) instead of ever calling $linecode
    #  if the existing file does not exist;
    #  if it's false dies instead
    # any of these may return false, in which case we quit immediately
    # any of these except enoentcode may be undef to mean "noop"
    # if they all return true, we install the new file
    my $wf = wf_start(".topbloke/$metafile");
    my $call = sub {
	return 1 unless $_->[0];
	return 1 if $_->[0]($wf);
	wf_abort($wf);
	close FI;
	return 0;
    };
    return unless $call->($startcode);
    if (!open FI, '<', ".topbloke/$metafile") {
	die "$metafile $!" unless $!==ENOENT;
	die "$metafile $!" unless $enoentcode;
	return unless $call->($enoentcode);
    } else {
	while (<FI>) {
	    chomp or die;
	    return unless $call->($linecode);
	    wf($wf, "$_\n");
	}
	FI->error and die $!;
	close FI or die $!;
    }
    return unless $call->($endcode);
    wf_done($wf);
}
    

sub depsfile_add_dep ($$) {
    my ($depsfile, $depspec) = @_;
    metafile_process($depsfile, undef, sub {
	die "dep $depspec already set in $depsfile ?!" if $_ eq $depspec;
    }, sub {
	wf($_->[0], "$depspec\n");
    }, undef);
}

#----- general utilities -----

sub wf_start ($) {
    my ($path) = @_;
    my $fh = new IO::File "$path.tmp", '>' or die "create $path.tmp: $!\n";
    return [ $fh, $path ];
}

sub wf ($$) {
    my ($wf, $data) = @_;
    my ($fh, $path) = @$wf;
    print $fh $data or die "write $path.tmp: $!\n";
}

sub wf_abort ($) {
    my ($wf) = @_;
    my ($fh, $path) = @$wf;
    close $fh;
    unlink "$path.tmp" or die "remove $path.tmp: $!\n";
}

sub wf_done ($) {
    my ($wf) = @_;
    my ($fh, $path) = @$wf;
    close $fh or die "finish writing $path.tmp: $!\n";
    rename "$path.tmp", $path or die "install new $path: $!\n";
}

sub wf_contents ($$) {
    my ($path,$contents) = @_;
    my $wf = wf_start($path);
    wf($wf, $contents);
    wf_done($wf);
}

sub closeout () {
    STDOUT->error and die $!;
    close STDOUT or die $!;
}

1;
